﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{
    public partial class Form1 : Form
    {
        private Microsoft.Office.Interop.Excel.Application excel;

        public Form1()
        {
            try
            {
                data = new DataSet();
                InitializeComponent();
                tool = new ToolTip();
                tool.SetToolTip(button1, "Добавить");
                this.Controls.Add(button1);
                tool.SetToolTip(button_del, "Удалить");
                this.Controls.Add(button_del);
                tool.SetToolTip(button3, "Редактировать");
                this.Controls.Add(button3);
                tool.SetToolTip(button5, "Следующая таблица");
                this.Controls.Add(button5);
                tool.SetToolTip(button6, "Предыдущая таблица");
                this.Controls.Add(button6);
                tool.SetToolTip(button7, "О программе");
                this.Controls.Add(button7);
                tool.SetToolTip(button8, "Настройка");
                this.Controls.Add(button8);

                StreamReader str = new StreamReader("config.txt", Encoding.GetEncoding(1251));
                Speak.connect = str.ReadLine();

                SqlConnection connection = new SqlConnection(Speak.connect);
                Speak.grid = dataGridView1;

                // **************************************для представлений адаптеры
                string command = "SELECT  * FROM View_2";
                Sql_adapter_view = new SqlDataAdapter(command, connection);
                Sql_adapter_view.Fill(data, "Сотрудники службы");

                command = "Select * From View_1";
                Sql_adapter_view_1 = new SqlDataAdapter(command, connection);
                Sql_adapter_view_1.Fill(data, "Устройства");


                // ***************************************табличные адаптеры
                command = "Select * From Отдел";
                Sql_adapter_1 = new SqlDataAdapter(command, connection.ConnectionString);
                Sql_adapter_1.Fill(data, "Отдел");

                command = "Select * From Производители";
                Sql_adapter_2 = new SqlDataAdapter(command, connection.ConnectionString);
                Sql_adapter_2.Fill(data, "Производители");

                command = "Select * From [Тип Устройства]";
                Sql_adapter_3 = new SqlDataAdapter(command, connection.ConnectionString);
                Sql_adapter_3.Fill(data, "Тип_Устройства");

                command = "Select * From Свойства";
                Sql_adapter_4 = new SqlDataAdapter(command, connection.ConnectionString);
                Sql_adapter_4.Fill(data, "Свойства");

                command = "Select * From Параметры";
                Sql_adapter_5 = new SqlDataAdapter(command, connection.ConnectionString);
                Sql_adapter_5.Fill(data, "Параметры");

                command = "Select * From Сотрудники";
                Sql_adapter_6 = new SqlDataAdapter(command, connection.ConnectionString);
                Sql_adapter_6.Fill(data, "Сотрудники");

                command = "Select * From Устройство";
                Sql_adapter_7 = new SqlDataAdapter(command, connection.ConnectionString);
                Sql_adapter_7.Fill(data, "Устройство");

                comboBox1.Items.Add("Сотрудники службы");
                comboBox1.Items.Add("Устройства");
                comboBox1.SelectedIndex = 0;
                dataGridView1.DataSource = data.Tables[comboBox1.Text];           
            }
            catch
            {
                MessageBox.Show("Ошибка установления связи", "SQL ERROR",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = data.Tables[comboBox1.Text];
            if (comboBox1.Text == "Сотрудники службы")
                dataGridView1.Columns[0].Visible = false;
            else if (comboBox1.Text == "Устройства")
                dataGridView1.Columns[0].Visible = false;



        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)//добавление
        {
            Speak.flag = 100;
            Speak.dat = data;
            Form2 add = new Form2(this);
            add.ShowDialog();
            button2_Click(sender, e);
        }

        

        private void button3_Click(object sender, EventArgs e)//редактирование
        {
            try
            {
                if (dataGridView1.CurrentRow.Index > -1)
                {
                    Speak.mydata = data.Tables[comboBox1.Text];
                    Speak.dat = data;
                    Speak.Number_strl = dataGridView1.CurrentRow.Index;
                    Speak.flag = comboBox1.SelectedIndex;
                    Form2 add = new Form2(this);
                    add.ShowDialog();
                    button2_Click(sender, e);
                }
            }
            catch
            {
                MessageBox.Show("Выберите запись для редактирования", "Редактирование",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void button_del_Click(object sender, EventArgs e)//удаление
        {
            try
            {
                if (dataGridView1.CurrentRow == null) MessageBox.Show("Сначала выберете запись для удаления", "Удаление",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (dataGridView1.CurrentRow.Index > -1)
                {
                    if (comboBox1.Text == "Сотрудники службы")
                    {
                        if (MessageBox.Show("Вы действительно хотите удалить выбранную запись? Данная запись будет навсегда удалена из БД.", "Удаление", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            SqlConnection connection = new SqlConnection(Speak.connect);
                            SqlCommand myCmd = new SqlCommand();
                            connection.Open();
                            myCmd.Connection = connection;
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "del_record_sotr";
                            myCmd.Parameters.AddWithValue("@id", data.Tables[comboBox1.Text].Rows[dataGridView1.CurrentRow.Index][0]);
                            myCmd.ExecuteNonQuery();
                            connection.Close();
                            button2_Click(sender, e);
                            MessageBox.Show("Удаление записи выполнено.", "Удаление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Удаление записи отменено.", "Удаление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else if (comboBox1.Text == "Устройства")
                    {
                        if (MessageBox.Show("Вы действительно хотите удалить выбранную запись? Данная запись будет навсегда удалена из БД.", "Удаление", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            SqlConnection connection = new SqlConnection(Speak.connect);
                            SqlCommand myCmd = new SqlCommand();
                            connection.Open();
                            myCmd.Connection = connection;
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "del_record_yst";
                            myCmd.Parameters.AddWithValue("@id", data.Tables[comboBox1.Text].Rows[dataGridView1.CurrentRow.Index][0]);                           
                            myCmd.ExecuteNonQuery();
                            connection.Close();
                            button2_Click(sender, e);
                            MessageBox.Show("Удаление записи выполнено.", "Удаление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Удаление записи отменено.", "Удаление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Удаление записи не было выполнено.", "Удаление",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button5_Click(object sender, EventArgs e)//таблица вперед
        {
            switch (comboBox1.SelectedIndex)
            {
                case -1:
                    {
                        dataGridView1.DataSource = data.Tables[0];
                        dataGridView1.Columns[0].Visible = false;
                        comboBox1.SelectedIndex = 0;
                        break;
                    }
                case 0:
                    {
                        dataGridView1.DataSource = data.Tables[1];
                        dataGridView1.Columns[1].Visible = false;
                        comboBox1.SelectedIndex++;
                        break;
                    }
            }
        }

        private void button6_Click(object sender, EventArgs e)//таблица назад
        {

            switch (comboBox1.SelectedIndex)
            {
                case 1:
                    {
                        dataGridView1.DataSource = data.Tables[1];
                        dataGridView1.Columns[0].Visible = false;
                        comboBox1.SelectedIndex--;
                        break;
                    }
                case 0:
                    {
                        dataGridView1.DataSource = data.Tables[0];
                        dataGridView1.Columns[0].Visible = false;
                        break;
                    }

            }
        }

        private void button7_Click(object sender, EventArgs e)//о программе
        {
            MessageBox.Show("Данная программ была разработана в учебных целях. Разработчик студент группы Ипзк-19-1 Прищепа Александр Юрьевич", "О программе", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Speak.dat = data;
            Настройка add_static = new Настройка(this);
            add_static.ShowDialog();
            button2_Click(sender, e);
        }


        public void restForm4Data(int id)
        {
            SqlConnection connection = new SqlConnection(Speak.connect);
            SqlCommand myCmd = new SqlCommand();
            connection.Open();
            myCmd.Connection = connection;
            myCmd.CommandType = CommandType.StoredProcedure;
            myCmd.Parameters.AddWithValue("@id",id);
            myCmd.CommandText = "get_sv_vo";
            myCmd.ExecuteNonQuery();
            Sql_zapros_1 = new SqlDataAdapter(myCmd);
            Sql_zapros_1.Fill(data, "Свойств");
        }
        private void button9_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.Text == "Устройства")
                {
                    if (dataGridView1.CurrentRow.Index > -1)
                    {
                        restForm4Data(Int32.Parse(data.Tables[comboBox1.Text].Rows[dataGridView1.CurrentRow.Index][0].ToString()));
                        Speak.mydata = data.Tables[comboBox1.Text];
                        Speak.dat = data;
                        Speak.Number_strl = dataGridView1.CurrentRow.Index;
                        Speak.idd = Int32.Parse(data.Tables[comboBox1.Text].Rows[dataGridView1.CurrentRow.Index][0].ToString());
                        Speak.flag = comboBox1.SelectedIndex;
                        Form4 sv = new Form4(this);
                        sv.ShowDialog();
                        button2_Click(sender, e);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Выберите запись для редактирования", "Редактирование",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
