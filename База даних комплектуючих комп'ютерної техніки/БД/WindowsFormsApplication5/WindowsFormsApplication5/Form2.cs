﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{

    public partial class Form2 : Form
    {
        Form1 obnov;

        public Form2(Form1 obn)
        {
            obnov = obn;
            InitializeComponent();
            tool = new ToolTip();
            tool.SetToolTip(button1, "Добавить");
            this.Controls.Add(button1);
            tool.SetToolTip(button2, "Редактировать");
            this.Controls.Add(button2);
            tool.SetToolTip(button_отмена, "Отменить и выйти");
            this.Controls.Add(button_отмена);
            button2.Hide();
            Speak.idd = 1;
            button1.Show();
            tabPage1.Show();
            tabPage4.Show();


            comboBox5.DataSource = Speak.dat.Tables["Отдел"];
            comboBox5.ValueMember = "Отдел";

            comboBox1.DataSource = Speak.dat.Tables["Сотрудники"];
            comboBox1.ValueMember = "Фамилия";

            comboBox3.DataSource = Speak.dat.Tables["Производители"];
            comboBox3.ValueMember = "Производитель";

            comboBox2.DataSource = Speak.dat.Tables["Тип_Устройства"];
            comboBox2.ValueMember = "Тип устройства";

            if (Speak.flag == 0)
            {
                button2.Show();
                button1.Hide();
                tabPage1.Parent = null;
                textBox7.Text = Speak.mydata.Rows[Speak.Number_strl][4].ToString();
                textBox6.Text = Speak.mydata.Rows[Speak.Number_strl][5].ToString();
                textBox5.Text = Speak.mydata.Rows[Speak.Number_strl][6].ToString();
                textBox8.Text = Speak.mydata.Rows[Speak.Number_strl][2].ToString();
                textBox9.Text = Speak.mydata.Rows[Speak.Number_strl][3].ToString();
                comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
                comboBox5.Text = Speak.mydata.Rows[Speak.Number_strl][1].ToString();
                comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
                Speak.flag = 2;
            }
            else if (Speak.flag == 1)
            {
                button2.Show();
                button1.Hide();
                tabPage4.Parent = null;
                textBox1.Text = Speak.mydata.Rows[Speak.Number_strl][4].ToString();
                textBox2.Text = Speak.mydata.Rows[Speak.Number_strl][5].ToString();
                textBox3.Text = Speak.mydata.Rows[Speak.Number_strl][6].ToString();
                comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
                comboBox2.Text = Speak.mydata.Rows[Speak.Number_strl][1].ToString();
                comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
                comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
                comboBox3.Text = Speak.mydata.Rows[Speak.Number_strl][2].ToString();
                comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
                comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
                comboBox1.Text = Speak.mydata.Rows[Speak.Number_strl][3].ToString();
                comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
                dateTimePicker1.Value = Convert.ToDateTime(Speak.mydata.Rows[Speak.Number_strl][7]);
                Speak.flag = 3;
            }
            else
            {
                comboBox1.SelectedIndex = -1;
                comboBox2.SelectedIndex = -1;
                comboBox3.SelectedIndex = -1;
                comboBox5.SelectedIndex = -1;
            }
        }

        private void button1_Click(object sender, EventArgs e) //добавление
        {
            if (MessageBox.Show("Подтвердите добавление новой записи?", "Добавление", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    SqlConnection connection = new SqlConnection(Speak.connect);
                    SqlCommand myCmd = new SqlCommand();
                    connection.Open();
                    myCmd.Connection = connection;
                    if (tabControl1.SelectedIndex == 0)
                    {
                        if ((textBox1.Text.ToString().Length == 0) || (textBox2.Text.ToString().Length == 0)
                            || (textBox3.Text.ToString().Length == 0) || (comboBox1.Text.ToString().Length == 0)
                            || (comboBox1.Text.ToString().Length == 0) || (comboBox3.Text.ToString().Length == 0))
                            MessageBox.Show("Введите значение", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.Connection = connection;
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "add_record_yst";
                            myCmd.Parameters.AddWithValue("@i", textBox1.Text);
                            myCmd.Parameters.AddWithValue("@s", textBox2.Text);
                            myCmd.Parameters.AddWithValue("@n", textBox3.Text);
                            myCmd.Parameters.AddWithValue("@t", comboBox2.Text.ToString());
                            myCmd.Parameters.AddWithValue("@p", comboBox3.Text.ToString());
                            myCmd.Parameters.AddWithValue("@d", dateTimePicker1.Value.Date);
                            myCmd.Parameters.AddWithValue("@f", comboBox1.Text.ToString());
                            myCmd.ExecuteNonQuery();
                            connection.Close();
                            MessageBox.Show("Добавление записи выполнено.", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }

                    }
                    else if (tabControl1.SelectedIndex == 1)
                    {
                        if ((textBox5.Text.ToString().Length == 0) || (textBox6.Text.ToString().Length == 0)
                            || (textBox7.Text.ToString().Length == 0) || (textBox8.Text.ToString().Length == 0)
                            || (textBox9.Text.ToString().Length == 0) || (comboBox5.Text.ToString().Length == 0))
                            MessageBox.Show("Введите значение", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.Connection = connection;
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "add_record_sotr";
                            myCmd.Parameters.AddWithValue("@f", textBox7.Text);
                            myCmd.Parameters.AddWithValue("@n", textBox6.Text);
                            myCmd.Parameters.AddWithValue("@s", textBox5.Text);
                            myCmd.Parameters.AddWithValue("@t", Int32.Parse(textBox8.Text));
                            myCmd.Parameters.AddWithValue("@d", textBox9.Text);
                            myCmd.Parameters.AddWithValue("@o", comboBox5.Text);
                            myCmd.ExecuteNonQuery();
                            connection.Close();
                            MessageBox.Show("Добавление записи выполнено.", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }

                }
                catch
                {
                    MessageBox.Show("Добавление записи не было выполнено. Возможно были введены неккоректные данные.", "Добавление",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button_edit_Click(object sender, EventArgs e) ////////редактирование
        {
            if (MessageBox.Show("Подтвердите редактирование записи?", "Редактирование", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    SqlConnection connection = new SqlConnection(Speak.connect);
                    SqlCommand myCmd = new SqlCommand();
                    connection.Open();
                    myCmd.Connection = connection;
                    if (Speak.flag == 3)
                    {
                        if ((textBox1.Text.ToString().Length == 0) || (textBox2.Text.ToString().Length == 0)
                            || (textBox3.Text.ToString().Length == 0) || (comboBox1.Text.ToString().Length == 0)
                            || (comboBox1.Text.ToString().Length == 0) || (comboBox3.Text.ToString().Length == 0))
                            MessageBox.Show("Введите значение", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.Connection = connection;
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "up_record_yst";
                            myCmd.Parameters.AddWithValue("@id", Speak.mydata.Rows[Speak.Number_strl][0]);
                            myCmd.Parameters.AddWithValue("@i", textBox1.Text);
                            myCmd.Parameters.AddWithValue("@s", textBox2.Text);
                            myCmd.Parameters.AddWithValue("@n", textBox3.Text);
                            myCmd.Parameters.AddWithValue("@t", comboBox2.Text.ToString());
                            myCmd.Parameters.AddWithValue("@p", comboBox3.Text.ToString());
                            myCmd.Parameters.AddWithValue("@d", dateTimePicker1.Value.Date);
                            myCmd.Parameters.AddWithValue("@f", comboBox1.Text.ToString());
                            myCmd.ExecuteNonQuery();
                            connection.Close();
                            MessageBox.Show("Добавление записи выполнено.", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }

                    }
                    else if (Speak.flag == 2)
                    {
                        if ((textBox5.Text.ToString().Length == 0) || (textBox6.Text.ToString().Length == 0)
                            || (textBox7.Text.ToString().Length == 0) || (textBox8.Text.ToString().Length == 0)
                            || (textBox9.Text.ToString().Length == 0) || (comboBox5.Text.ToString().Length == 0))
                            MessageBox.Show("Введите значение", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.Connection = connection;
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "up_record_sotr";
                            myCmd.Parameters.AddWithValue("@id", Speak.mydata.Rows[Speak.Number_strl][0]);
                            myCmd.Parameters.AddWithValue("@f", textBox7.Text);
                            myCmd.Parameters.AddWithValue("@n", textBox6.Text);
                            myCmd.Parameters.AddWithValue("@s", textBox5.Text);
                            myCmd.Parameters.AddWithValue("@t", Int32.Parse(textBox8.Text));
                            myCmd.Parameters.AddWithValue("@d", textBox9.Text);
                            myCmd.Parameters.AddWithValue("@o", comboBox5.Text);
                            myCmd.ExecuteNonQuery();
                            connection.Close();
                            MessageBox.Show("Добавление записи выполнено.", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Редактирование записи не было выполнено. Возможно были введены неккоректные данные.", "Редактирование",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void button_отмена_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Отменить и выйти?", "Выход", MessageBoxButtons.YesNo) == DialogResult.Yes)
                this.Close();
        }


        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
                e.Handled = true;
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
                e.Handled = true;
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
                e.Handled = true;
        }


    }
}