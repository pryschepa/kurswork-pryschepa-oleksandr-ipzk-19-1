﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using System.Data.Sql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{
    public partial class Настройка : Form
    {
        Form1 obnov;
        public Настройка(Form1 obn)
        {
            obnov = obn;
            InitializeComponent();
            tool = new ToolTip();
            tool.SetToolTip(button1, "Добавить");
            this.Controls.Add(button1);
            tool.SetToolTip(button_del, "Удалить");
            this.Controls.Add(button_del);
            tool.SetToolTip(button3, "Редактировать");
            this.Controls.Add(button3);
            tool.SetToolTip(button2, "Подтвердить");
            this.Controls.Add(button2);
            tool.SetToolTip(button5, "Выйти");
            this.Controls.Add(button5);
            dataGridView1.DataSource = Speak.dat.Tables["Отдел"];
            dataGridView2.DataSource = Speak.dat.Tables["Производители"];
            dataGridView3.DataSource = Speak.dat.Tables["Тип_Устройства"];
            dataGridView4.DataSource = Speak.dat.Tables["Параметры"];
            dataGridView1.Columns[0].Visible = false;
            dataGridView2.Columns[0].Visible = false;
            dataGridView3.Columns[0].Visible = false;
            dataGridView4.Columns[0].Visible = false;
            button2.Hide();

        }

        private void button1_Click(object sender, EventArgs e)//добавление
        {
            if (MessageBox.Show("Подтвердите добавление новой записи?", "Добавление", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    SqlConnection connection = new SqlConnection(Speak.connect);
                    SqlCommand myCmd_1 = new SqlCommand();
                    SqlCommand myCmd = new SqlCommand();
                    connection.Open();
                    myCmd.Connection = connection;
                    if (tabControl5.SelectedIndex == 0)
                    {
                        if (textBox1.Text.ToString().Length == 0)
                            MessageBox.Show("Введите значение", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "add_record_otdel";
                            myCmd.Parameters.AddWithValue("@value", textBox1.Text.ToString());

                            SqlParameter par = new SqlParameter("ret", SqlDbType.Int);
                            par.Direction = ParameterDirection.ReturnValue;
                            myCmd.Parameters.Add(par);
                            myCmd.ExecuteNonQuery();
                            if (Convert.ToInt32(par.Value) == 1)
                                MessageBox.Show("Данная запись уже существует в БД", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else
                                MessageBox.Show("Добавление записи выполнено.", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            textBox1.Clear();
                            obnov.button2_Click(sender, e);
                        }
                    }
                    else if (tabControl5.SelectedIndex == 1)
                    {
                        if (textBox2.Text.ToString().Length == 0)
                            MessageBox.Show("Введите значение", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "add_record_proiz";
                            myCmd.Parameters.AddWithValue("@value", textBox2.Text.ToString());
                            SqlParameter par = new SqlParameter("ret", SqlDbType.Int);
                            par.Direction = ParameterDirection.ReturnValue;
                            myCmd.Parameters.Add(par);
                            myCmd.ExecuteNonQuery();
                            if (Convert.ToInt32(par.Value) == 1)
                                MessageBox.Show("Данная запись уже существует в БД", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else
                                MessageBox.Show("Добавление записи выполнено.", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            textBox2.Clear();
                            obnov.button2_Click(sender, e);
                        }
                    }
                    else if (tabControl5.SelectedIndex == 2)
                    {
                        if (textBox3.Text.ToString().Length == 0)
                            MessageBox.Show("Введите значение", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "add_record_ty";
                            myCmd.Parameters.AddWithValue("@value", textBox3.Text.ToString());

                            SqlParameter par = new SqlParameter("ret", SqlDbType.Int);
                            par.Direction = ParameterDirection.ReturnValue;
                            myCmd.Parameters.Add(par);
                            myCmd.ExecuteNonQuery();
                            if (Convert.ToInt32(par.Value) == 1)
                                MessageBox.Show("Данная запись уже существует в БД", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else
                                MessageBox.Show("Добавление записи выполнено.", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            textBox3.Clear();
                            obnov.button2_Click(sender, e);
                        }
                    }
                    else if (tabControl5.SelectedIndex == 3)
                    {
                        if (textBox4.Text.ToString().Length == 0)
                            MessageBox.Show("Введите значение", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "add_record_par";
                            myCmd.Parameters.AddWithValue("@value", textBox4.Text.ToString());

                            SqlParameter par = new SqlParameter("ret", SqlDbType.Int);
                            par.Direction = ParameterDirection.ReturnValue;
                            myCmd.Parameters.Add(par);
                            myCmd.ExecuteNonQuery();
                            if (Convert.ToInt32(par.Value) == 1)
                                MessageBox.Show("Данная запись уже существует в БД", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else
                                MessageBox.Show("Добавление записи выполнено.", "Добавление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            textBox4.Clear();
                            obnov.button2_Click(sender, e);
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Добавление записи не было выполнено. Возможно были введены неккоректные данные.", "Добавление",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void button_del_Click(object sender, EventArgs e)//удаление
        {
            if (dataGridView1.CurrentRow.Index > -1)
            {
                try
                {
                    if (dataGridView1.CurrentRow.Index > -1)
                    {
                        if (tabControl5.SelectedIndex == 0)
                        {
                            SqlConnection connection = new SqlConnection(Speak.connect);
                            SqlCommand myCmd = new SqlCommand();
                            connection.Open();
                            myCmd.Connection = connection;
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "del_record_otdel";
                            myCmd.Parameters.AddWithValue("@id", Speak.dat.Tables["Отдел"].Rows[dataGridView1.CurrentRow.Index][0]);
                            myCmd.ExecuteNonQuery();
                            MessageBox.Show("Удаление записи выполнено.", "Удаление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            obnov.button2_Click(sender, e);
                        }
                        else if (tabControl5.SelectedIndex == 1)
                        {
                            SqlConnection connection = new SqlConnection(Speak.connect);
                            SqlCommand myCmd = new SqlCommand();
                            connection.Open();
                            myCmd.Connection = connection;
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "del_record_proiz";
                            myCmd.Parameters.AddWithValue("@id", Speak.dat.Tables["Производители"].Rows[dataGridView2.CurrentRow.Index][0]);
                            myCmd.ExecuteNonQuery();
                            MessageBox.Show("Удаление записи выполнено.", "Удаление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            obnov.button2_Click(sender, e);
                        }
                        else if (tabControl5.SelectedIndex == 2)
                        {
                            SqlConnection connection = new SqlConnection(Speak.connect);
                            SqlCommand myCmd = new SqlCommand();
                            connection.Open();
                            myCmd.Connection = connection;
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "del_record_ty";
                            myCmd.Parameters.AddWithValue("@id", Speak.dat.Tables["Тип_Устройства"].Rows[dataGridView3.CurrentRow.Index][0]);
                            myCmd.ExecuteNonQuery();
                            MessageBox.Show("Удаление записи выполнено.", "Удаление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            obnov.button2_Click(sender, e);
                        }
                        else if (tabControl5.SelectedIndex == 3)
                        {

                            SqlConnection connection = new SqlConnection(Speak.connect);
                            SqlCommand myCmd = new SqlCommand();
                            connection.Open();
                            myCmd.Connection = connection;
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "del_record_par";
                            myCmd.Parameters.AddWithValue("@id", Speak.dat.Tables["Параметры"].Rows[dataGridView3.CurrentRow.Index][0]);
                            myCmd.ExecuteNonQuery();
                            MessageBox.Show("Удаление записи выполнено.", "Удаление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            obnov.button2_Click(sender, e);
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Удаление записи не было выполнено.", "Удаление",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)//редактирование
        {
            if (dataGridView1.CurrentRow.Index > -1)
            {
                button2.Show();
                button3.Hide();
                button5.Hide();
                button_del.Hide();
                button1.Hide();
                if (tabControl5.SelectedIndex == 0)
                    textBox1.Text = Speak.dat.Tables["Отдел"].Rows[dataGridView1.CurrentRow.Index][1].ToString();
                else if (tabControl5.SelectedIndex == 1)
                    textBox2.Text = Speak.dat.Tables["Производители"].Rows[dataGridView2.CurrentRow.Index][1].ToString();
                else if (tabControl5.SelectedIndex == 2)
                    textBox3.Text = Speak.dat.Tables["Тип_Устройства"].Rows[dataGridView3.CurrentRow.Index][1].ToString();
                else if (tabControl5.SelectedIndex == 3)
                    textBox4.Text = Speak.dat.Tables["Параметры"].Rows[dataGridView4.CurrentRow.Index][1].ToString();

            }
        }

        private void button2_Click(object sender, EventArgs e)//подтверждение
        {
            if (MessageBox.Show("Подтвердите редактирование записи?", "Редактирование", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    SqlConnection connection = new SqlConnection(Speak.connect);
                    SqlCommand myCmd_1 = new SqlCommand();
                    SqlCommand myCmd = new SqlCommand();
                    connection.Open();
                    myCmd.Connection = connection;
                    if (tabControl5.SelectedIndex == 0)
                    {
                        if (textBox1.Text.ToString().Length == 0)
                            MessageBox.Show("Введите значение", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "up_record_otdel";
                            myCmd.Parameters.AddWithValue("@id", Speak.dat.Tables["Отдел"].Rows[dataGridView1.CurrentRow.Index][0]);
                            myCmd.Parameters.AddWithValue("@value", textBox1.Text.ToString());
                            SqlParameter par = new SqlParameter("ret", SqlDbType.Int);
                            par.Direction = ParameterDirection.ReturnValue;
                            myCmd.Parameters.Add(par);
                            myCmd.ExecuteNonQuery();
                            if (Convert.ToInt32(par.Value) == 1)
                                MessageBox.Show("Данная запись уже существует в БД", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else
                                MessageBox.Show("Редактирование записи выполнено.", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            textBox1.Clear();
                            obnov.button2_Click(sender, e);
                        }
                    }
                    else if (tabControl5.SelectedIndex == 1)
                    {
                        if (textBox2.Text.ToString().Length == 0)
                            MessageBox.Show("Введите значение", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "up_record_proiz";
                            myCmd.Parameters.AddWithValue("@id", Speak.dat.Tables["Производители"].Rows[dataGridView2.CurrentRow.Index][0]);
                            myCmd.Parameters.AddWithValue("@value", textBox2.Text.ToString());
                            SqlParameter par = new SqlParameter("ret", SqlDbType.Int);
                            par.Direction = ParameterDirection.ReturnValue;
                            myCmd.Parameters.Add(par);
                            myCmd.ExecuteNonQuery();
                            if (Convert.ToInt32(par.Value) == 1)
                                MessageBox.Show("Данная запись уже существует в БД", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else
                                MessageBox.Show("Редактирование записи выполнено.", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            textBox2.Clear();
                            obnov.button2_Click(sender, e);
                        }
                    }
                    else if (tabControl5.SelectedIndex == 2)
                    {
                        if (textBox3.Text.ToString().Length == 0)
                            MessageBox.Show("Введите значение", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "up_record_ty";
                            myCmd.Parameters.AddWithValue("@id", Speak.dat.Tables["Тип_Устройства"].Rows[dataGridView3.CurrentRow.Index][0]);
                            myCmd.Parameters.AddWithValue("@value", textBox3.Text.ToString());
                            SqlParameter par = new SqlParameter("ret", SqlDbType.Int);
                            par.Direction = ParameterDirection.ReturnValue;
                            myCmd.Parameters.Add(par);
                            myCmd.ExecuteNonQuery();
                            if (Convert.ToInt32(par.Value) == 1)
                                MessageBox.Show("Данная запись уже существует в БД", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else
                                MessageBox.Show("Редактирование записи выполнено.", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            textBox3.Clear();
                            obnov.button2_Click(sender, e);
                        }
                    }
                    else if (tabControl5.SelectedIndex == 3)
                    {
                        if (textBox4.Text.ToString().Length == 0)
                            MessageBox.Show("Введите значение", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            myCmd.CommandType = CommandType.StoredProcedure;
                            myCmd.CommandText = "up_record_par";
                            myCmd.Parameters.AddWithValue("@id", Speak.dat.Tables["Параметры"].Rows[dataGridView4.CurrentRow.Index][0]);
                            myCmd.Parameters.AddWithValue("@value", textBox4.Text.ToString());
                            SqlParameter par = new SqlParameter("ret", SqlDbType.Int);
                            par.Direction = ParameterDirection.ReturnValue;
                            myCmd.Parameters.Add(par);
                            myCmd.ExecuteNonQuery();
                            if (Convert.ToInt32(par.Value) == 1)
                                MessageBox.Show("Данная запись уже существует в БД", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else
                                MessageBox.Show("Редактирование записи выполнено.", "Редактирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            connection.Close();
                            textBox4.Clear();
                            obnov.button2_Click(sender, e);
                        }
                    }

                }
                catch
                {
                    MessageBox.Show("Редактирование записи не было выполнено. Возможно были введены неккоректные данные.", "Редактирование",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            button2.Hide();
            button3.Show();
            button5.Show();
            button_del.Show();
            button1.Show();
        }

        private void button5_Click(object sender, EventArgs e)//выход
        {
            if (MessageBox.Show("Вы действительно желаете выйти?", "Выход", MessageBoxButtons.YesNo) == DialogResult.Yes)
                this.Close();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

    }
}
