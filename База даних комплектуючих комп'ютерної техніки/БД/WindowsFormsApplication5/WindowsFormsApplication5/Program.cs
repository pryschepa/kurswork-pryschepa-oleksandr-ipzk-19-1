﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Sql;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{

    static class Speak
    {
        public static DataTable mydata { get; set; }
        public static DataSet dat { get; set; }
        public static int Number_strl { get; set; }
        public static int flag { get; set; }
        public static int idd { get; set; }
        public static int flag_comb { get; set; }
        public static int flag_print { get; set; }
        public static int flag_text { get; set; }
        public static int parametr { get; set; }
        public static string parametr_str { get; set; }
        public static string parametr_str_1 { get; set; }
        public static string connect { get; set; }
        public static Form1 form { get; set; }
        public static DataGridView grid { get; set; }
    }

    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
